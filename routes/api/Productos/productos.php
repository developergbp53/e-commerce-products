<?php
Route::group(['middleware' => ['jwt.auth'], 'prefix' => 'productos' ], function () {
    Route::get('/', 'productosController@listaProductos');
    Route::post('/create', 'productosController@crearProductos');
    Route::get('/{id_producto}','productosController@verProductos');
    Route::delete('/eliminar/{id}','productosController@eliminarProducto');
    Route::put('/editar/{id}','productosController@editarProducto');
    Route::get('/buscar/{buscar}','productosController@busquedaProducto');
    Route::get('/tallas/{id}', 'productosController@getTallas');
    Route::get('/atributos/{id}', 'productosController@getAtributos');
   

});

Route::group(['middleware' => ['jwt.auth'], 'prefix' => 'order' ], function () {
Route::get('/name/product', 'productosController@orderByName');
Route::get('/update/product', 'productosController@orderByUpdate');
Route::get('/category/product/{id}', 'productosController@orderByCAtegory');
});