<?php


Route::group(['middleware' => ['jwt.auth'], 'prefix' => 'atributos-imagenes'], function () {
  Route::post('/create', 'productosAtributosImagenesController@crearImagenAtributo');
  Route::get('/{id}', 'productosAtributosImagenesController@showImages');
  Route::get('/images/{id}', 'productosAtributosImagenesController@showImage');
  Route::delete('/delete/{id}','productosAtributosImagenesController@eliminarImagen');

}); 

?>