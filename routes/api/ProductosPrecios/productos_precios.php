<?php


Route::group(['middleware' => ['jwt.auth'], 'prefix' => 'precios'], function () {

  Route::post('/create', 'productosPreciosController@crearProductos_precio');
  Route::get('/{id}', 'productosPreciosController@getPrecioPorId');
  Route::put('/editar/{id}','productosPreciosController@editarPrecio');
  Route::delete('/eliminar/{id}','productosPreciosController@eliminarPrecio');

  
});