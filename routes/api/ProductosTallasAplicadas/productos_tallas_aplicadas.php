<?php


Route::group(['middleware' => ['jwt.auth'], 'prefix' => 'tallas/productos'], function () {

   Route::post('/create', 'productosTallasAplicadasController@crearProductosTallas');
   Route::post('/edit/create', 'productosTallasAplicadasController@editCreate');
   Route::post('/edit/delete', 'productosTallasAplicadasController@editDelete');


  
});