<?php
Route::post('login', 'loginController@authenticate');

Route::group(['middleware' => ['jwt.auth'], 'prefix' => 'admin' ], function () {

  Route::get('/', 'AdminsController@listasAdministradores');
  Route::post('/create', 'AdminsController@crearAdministradores');
  Route::get('/{id}','AdminsController@verAdministradores');
  Route::put('/editar/{id}','AdminsController@editarAdministradores');
  Route::delete('/eliminar/{id}','AdminsController@eliminarAdministradores');
  Route::get('/buscar/{buscar}','AdminsController@busquedaAdmin');

  
});