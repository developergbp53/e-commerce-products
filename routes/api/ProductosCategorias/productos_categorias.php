<?php


Route::group(['middleware' => ['jwt.auth'], 'prefix' => 'categorias'], function () {

  Route::post('/create', 'productosCategoriasController@crearProductos_categoria');
  Route::get('/', 'productosCategoriasController@listarCategorias');
  Route::post('/editar/{id}','productosCategoriasController@editarCategoria');
  Route::delete('/eliminar/{id}','productosCategoriasController@eliminarCategoria');

  
});