<?php


Route::group(['middleware' => ['jwt.auth'], 'prefix' => 'atributos' ], function () {

   Route::post('/create', 'productosAtributosController@crearAtributo');
   Route::post('/subatributos/create', 'productosAtributosController@crearSubAtributo');
   Route::get('/', 'productosAtributosController@getAtributosPadre');
   Route::put('/editar/{id}','productosAtributosController@editarAtributo');
   Route::delete('/eliminar/{id}','productosAtributosController@eliminarAtributo');

  
});