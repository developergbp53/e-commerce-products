<?php


Route::group(['middleware' => ['jwt.auth'], 'prefix' => 'tallas' ], function () {

  Route::post('create', 'productosTallasController@crearTalla');
  Route::post('/subtallas/create', 'productosTallasController@crearSubTalla');
  Route::get('/', 'productosTallasController@getTallaPadre');
  Route::put('/editar/{id}','productosTallasController@editarTalla');
  Route::delete('/eliminar/{id}','productosTallasController@eliminarTalla');
  Route::post('/position', 'productosTallasController@actualizarPosicionTalla');


  
});