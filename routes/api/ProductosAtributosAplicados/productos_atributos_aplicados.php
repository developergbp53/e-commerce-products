<?php


Route::group(['middleware' => ['jwt.auth'], 'prefix' => 'atributos/productos' ], function () {

   Route::post('/create', 'productosAtributosAplicadosController@crearProductosAtributos');
   Route::post('/edit/create', 'productosAtributosAplicadosController@editCreate');
   Route::post('/edit/delete', 'productosAtributosAplicadosController@editDelete');

  
});