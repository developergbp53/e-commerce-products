<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::get('mi-primer-ruta', function (Request $request) {
  return 'hola mundo';
});


Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
