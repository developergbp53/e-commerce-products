<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ProductosCategoria;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class productosCategoriasController extends Controller
{
	function listarCategorias()
	{
		try {
			$categorias = DB::table('productos_categorias')
				->select('productos_categorias.nombre', 'productos_categorias.id', 'productos_categorias.id_talla')
				->selectRaw("(SELECT CONCAT('https://prueba-coex.s3.amazonaws.com/',icono)) as imagen")
				->selectRaw("(SELECT COUNT(t1.id) FROM productos_tallas AS t1 INNER JOIN productos_tallas AS t2 ON t2.id_padre = t1.id WHERE t1.id = productos_categorias.id_talla  GROUP BY t1.id) AS TallasCount")
				->orderBy('updated_at', 'desc')
				->get();;
			foreach ($categorias as $key => $item) {
				$tallaName = DB::table('productos_tallas')
					->select('productos_tallas.nombre')
					->where('productos_tallas.id', $item->id_talla)
					->first();
				$item->tallaName = $tallaName;
			}
			return [
				"categorias" => $categorias
			];
		} catch (\Throwable $th) {
			$this->capturar($th);
		}
	}
	function crearProductos_categoria(Request $request)
	{
		try {
			$validator = Validator::make($request->all(), [
				'nombre' => 'required',
				'id_talla' => 'required',
				'imagen' => 'required'
			]);
			if ($validator->fails()) {
				return response()->json($validator->errors()->toJson(), 400);
			};

			if ($request->imagen == 'null') {
				$img = 'categorias/beb53d9a19ad1c7aec17a67962b9eeb4.admin.svg';
			} else {
				$img = $this->getStoreFiles($request->imagen, 'categorias');
			}

			$producto_categoria = new ProductosCategoria;
			$producto_categoria->nombre = $request->nombre;
			$producto_categoria->id_talla = $request->id_talla;
			$producto_categoria->created_by = auth()->id();
			$producto_categoria->icono = $img;
			$producto_categoria->save();

			return response()->json(array('success' => true), 200);
		} catch (\Throwable $th) {
			return $this->capturar($th);
		}
	}
	function eliminarCategoria($id)
	{
		try {
			$categoriaDelete = ProductosCategoria::find($id);
			$categoriaDelete->delete();
			return response()->json(array('success' => true), 200);
		} catch (\Throwable $th) {
			return $this->capturar($th);
		}
	}
	function editarCategoria(Request $request, $id)
	{
		try {

			$categoriaFoto = DB::table('productos_categorias')
				->select('icono')
				->where('id', $id)
				->first();

			if ($request->imagen == 'null') {
				$img = $categoriaFoto->icono;
			} else {
				$img = $this->getStoreFiles($request->imagen, 'categorias');
			}

			DB::table('productos_categorias')
				->where('id', $id)
				->update(['nombre' => $request->nombre, 'id_talla' => $request->id_talla, 'updated_by' => auth()->id(), 'icono' => $img]);

			return response()->json(array('success' => true), 200);
		} catch (\Throwable $th) {
			return $this->capturar($th);
		}
	}
}
