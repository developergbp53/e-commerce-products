<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\productosAtributosImagene;
use Illuminate\Support\Facades\DB;

class productosAtributosImagenesController extends Controller
{
	public function crearImagenAtributo(Request $request)
	{
		try {
			$img = $this->guardarImagen($request->imagen, 800, "atributos", true, 100);


			$imagen = new productosAtributosImagene;
			$imagen->id_atributo = $request->idAtributo;
			$imagen->created_by = auth()->id();
			$imagen->id_producto = $request->idProducto;
			$imagen->imagen = $img['ruta'];
			$imagen->imagen_mini = $img['ruta_mini'];
			$imagen->save();

			return response()->json(array('success' => true), 200);;
		} catch (\Throwable $th) {
			$this->capturar($th);
		}
	}

	public function showImages($id)
	{
		try {

			$atributos = DB::table('productos_atributos_aplicados')
				->select('pa.nombre', 'pa.id')
				->join('productos_atributos AS pa', 'pa.id', '=', 'productos_atributos_aplicados.id_atributo')
				->where('productos_atributos_aplicados.id_producto', $id)
				->get();

			foreach ($atributos as $key => $item) {
				$imgs =  DB::table('productos')
					->select('productos_atributos_imagenes.id_producto', 'productos_atributos_imagenes.id', 'productos_atributos_imagenes.imagen', 'productos_atributos_imagenes.id_atributo')
					->selectRaw("(SELECT CONCAT('https://prueba-coex.s3.amazonaws.com/',imagen)) as imagen")
					->join('productos_atributos_aplicados', 'productos.id', '=', 'productos_atributos_aplicados.id_producto')
					->join('productos_atributos', 'productos_atributos.id', '=', 'productos_atributos_aplicados.id_atributo')
					->join('productos_atributos_imagenes', 'productos_atributos.id', '=', 'productos_atributos_imagenes.id_atributo')
					->where('productos_atributos_imagenes.id_producto', $id)
					->where('productos_atributos_aplicados.id_atributo', $item->id)
					->groupBy('productos_atributos_imagenes.id')
					->get();
				$item->imagenes = $imgs;
			}

			return ['slides' => $atributos];
		} catch (\Throwable $th) {
			$this->capturar($th);;
		}
	}
	function eliminarImagen($id)
	{
		try {
			$imageDelete = productosAtributosImagene::find($id);
			$imageDelete->delete();
			return response()->json(array('success' => true), 200);
		} catch (\Throwable $th) {
			return $this->capturar($th);
		}
	}
	public function showImage($id)
	{
		try {

			$imgs =  DB::table('productos')
				->select('productos_atributos_imagenes.id_producto', 'productos_atributos_imagenes.imagen', 'productos_atributos_imagenes.id_atributo')
				->selectRaw("(SELECT CONCAT('https://prueba-coex.s3.amazonaws.com/',imagen)) as imagen")
				->join('productos_atributos_aplicados', 'productos.id', '=', 'productos_atributos_aplicados.id_producto')
				->join('productos_atributos', 'productos_atributos.id', '=', 'productos_atributos_aplicados.id_atributo')
				->join('productos_atributos_imagenes', 'productos_atributos.id', '=', 'productos_atributos_imagenes.id_atributo')
				->where('productos_atributos_imagenes.id_producto', $id)
				->groupBy('productos_atributos_imagenes.id')
				->get();
			return ['slides' => $imgs];
		} catch (\Throwable $th) {
			$this->capturar($th);;
		}
	}
}
