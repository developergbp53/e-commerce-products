<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ProductosTalla;
use Illuminate\Support\Facades\DB;
use LengthException;

class productosTallasController extends Controller
{
	function crearTalla(Request $request)
	{
		try {
			$producto_talla = new ProductosTalla;
			$producto_talla->nombre = $request->atributoNombre;
			$producto_talla->posicion = $request->posicion;
			$producto_talla->created_by = auth()->id();
			$producto_talla->save();
		} catch (\Exception $e) {
			return $this->capturar($e, 'Error');
		}
	}
	function crearSubTalla(Request $request)
	{
		try {
			$producto_talla = new ProductosTalla;
			$producto_talla->nombre = $request->subTallaNombre;
			$producto_talla->posicion = $request->posicion;
			$producto_talla->id_padre = $request->id_padre;
			$producto_talla->created_by = auth()->id();
			$producto_talla->save();
		} catch (\Exception $e) {
			return $this->capturar($e, 'Error');
		}
	}

	public function getTallaPadre()
	{
		try {
			$tallaPadre = DB::table('productos_tallas')
				->select('productos_tallas.id', 'productos_tallas.nombre', 'productos_tallas.posicion')
				->where('productos_tallas.id_padre', null)
				->get();
			foreach ($tallaPadre as $key => $item) {
				$tallaHijos = DB::table('productos_tallas')
					->select('productos_tallas.id', 'productos_tallas.id_padre', 'productos_tallas.nombre', 'productos_tallas.posicion')
					->selectRaw("(SELECT COUNT(id_producto) FROM productos_tallas_aplicadas WHERE productos_tallas.id = productos_tallas_aplicadas.id_talla  GROUP BY id_talla) AS ProductosCount")
					->where('productos_tallas.id_padre', $item->id)
					->get();
				$item->subTallas = $tallaHijos;
			}
			return ['tallas' => $tallaPadre];
		} catch (\Exception $e) {
			return $this->capturar($e, 'Error');
		}
	}
	public function eliminarTalla($id)
	{
		try {
			$tallaDelete = ProductosTalla::find($id);
			$tallaDelete->delete();
		} catch (\Exception $e) {
			return $this->capturar($e, 'Error');
		}
	}
	public function editarTalla(Request $request, $id)
	{
		try {
			$tallaEdit = ProductosTalla::FindOrFail($id);
			$tallaEdit->nombre = $request->nombre;
			$tallaEdit->posicion = $request->posicion;
			$tallaEdit->updated_by = auth()->id();
			$tallaEdit->save();
		} catch (\Exception $e) {
			return $this->capturar($e, 'Error');
		}
	}
	public function actualizarPosicionTalla(Request $request)
	{
		try {

			$allPositions = DB::table('productos_tallas')
				->select('productos_tallas.id', 'productos_tallas.nombre', 'productos_tallas.posicion')
				->where('productos_tallas.id_padre', null)
				->get();

			if ($request->nuevaPosicion > $request->antiguaPosicion) {
				foreach ($allPositions as $key => $item) {
					if ($item->posicion == $request->antiguaPosicion) {
						$update = ProductosTalla::find($item->id);
						$update->posicion = $request->nuevaPosicion;
						$update->save();
						continue;
					};
					if ($item->posicion <= $request->nuevaPosicion) {
						$update = ProductosTalla::find($item->id);
						$update->posicion = $item->posicion - 1;
						$update->save();
					}
				}
			}
			if ($request->nuevaPosicion < $request->antiguaPosicion) {
				foreach ($allPositions as $key => $item) {
					if ($item->posicion == $request->antiguaPosicion) {
						$update = ProductosTalla::find($item->id);
						$update->posicion = $request->nuevaPosicion;
						$update->save();
					}
					if ($item->posicion < $request->antiguaPosicion && $item->posicion < $request->antiguaPosicion) {
						$update = ProductosTalla::find($item->id);
						$update->posicion = $item->posicion + 1;
						$update->save();
					}
				}
			}

			return response()->json(array('success' => true), 200);
		} catch (\Exception $e) {
			return $this->capturar($e, 'Error');
		}
	}
}
