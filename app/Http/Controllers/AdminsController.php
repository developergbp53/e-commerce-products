<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;


class AdminsController extends Controller
{
	function listasAdministradores()
	{
		try {
			$administradores = DB::table('users')
				->select('users.nombres', 'users.id', 'imagen', 'observaciones')
				->orderBy('updated_at', 'desc')
				->get();
			foreach ($administradores as $key => $item) {
				if (!is_null($item->imagen)) {
					$item->foto = "https://prueba-coex.s3.amazonaws.com/" . "$item->imagen";
				}
			}

			return [
				"administradores" => $administradores
			];
		} catch (\Throwable $th) {
			return $this->capturar($th);
		}
	}
	function crearAdministradores(Request $request)
	{
		try {

			$validator = Validator::make($request->all(), [
				'nombre' => 'required',
				'apellidos' => 'required',
				'correoElectronico' => 'required|email|unique:users,correo',
				'contrasena' => 'required|min:7|',
				'telefono' => 'required|min:11|numeric',
				'contrasena_confirmation' => 'required|min:7|',
			]);
			if ($validator->fails()) {
				return response()->json($validator->errors()->toJson(), 400);
			}
			!is_null($request->foto) ? $img = $this->guardarImagen($request->foto, 800, "usuarios") : $img['ruta'] = 'usuarios/446941402628259830b7da3.874900990.047072001652709763.jpeg';


			$admin = new User;
			$admin->nombres = $request->nombre;
			$admin->correo = $request->correoElectronico;
			$admin->password = bcrypt($request->contrasena);
			$admin->apellidos = $request->apellidos;
			$admin->observaciones = $request->acerca;
			$admin->telefono = $request->telefono;
			$admin->tipo = $request->tipo;
			$admin->imagen = $img['ruta'];
			$admin->save();
			return response()->json(array('success' => true), 200);
		} catch (\Throwable $th) {
			return $this->capturar($th);
		}
	}
	function verAdministradores($id)
	{
		try {
			$admindetail = DB::table('users')
				->select('users.nombres', 'users.apellidos', 'users.observaciones', 'users.id', 'imagen', 'correo', 'telefono', 'tipo')
				->where('users.id', $id)
				->get();
			foreach ($admindetail as $key => $item) {
				if (!is_null($item->imagen)) {
					$item->foto = "https://prueba-coex.s3.amazonaws.com/" . "$item->imagen";
				}
			}
			return ['admindetail' => $admindetail];
		} catch (\Throwable $th) {
			return $this->capturar($th);
		};
	}
	function editarAdministradores(Request $request, $id)
	{
		try {

			$validator = Validator::make($request->all(), [
				'nombre' => 'required',
				'apellidos' => 'required',
				'correoElectronico' => 'required|email|',
				'contrasena' => 'required',
				'telefono' => 'required|min:11|numeric',
			]);
			if ($validator->fails()) {
				return response()->json($validator->errors()->toJson(), 400);
			}
			$adminFoto = DB::table('users')
				->select('imagen')
				->where('users.id', $id)
				->first();
			!is_null($request->foto) ? $img = $this->guardarImagen($request->foto, 800, "usuarios") : $img['ruta'] = $adminFoto->imagen;


			$adminUpdate = User::FindOrFail($id);
			$adminUpdate->nombres = $request->nombre;
			$adminUpdate->correo = $request->correoElectronico;
			$adminUpdate->password = bcrypt($request->contrasena);
			$adminUpdate->apellidos = $request->apellidos;
			$adminUpdate->observaciones = $request->acerca;
			$adminUpdate->telefono = $request->telefono;
			$adminUpdate->tipo = $request->tipo;
			$adminUpdate->imagen = $img['ruta'];
			$adminUpdate->save();
			return response()->json(array('success' => true), 200);
		} catch (\Throwable $th) {
			return $this->capturar($th);
		}
	}
	function eliminarAdministradores($id)
	{
		try {
			$adminDelete = User::find($id);
			$adminDelete->delete();
			return response()->json(array('success' => true), 200);
		} catch (\Throwable $th) {
			return $this->capturar($th);
		}
	}
	function busquedaAdmin($buscar)
	{
		try {
			$users_busqueda = DB::table('users')
				->select('*')
				->where('users.nombres', 'like', '%' . $buscar . '%')
				->get();
			return ['administradores' => $users_busqueda];
		} catch (\Throwable $th) {
			return $this->capturar($th);
		}
	}
}
