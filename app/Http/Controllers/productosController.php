<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Producto;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class productosController extends Controller
{
	function listaProductos()
	{
		try {
			$productos = DB::table('productos')
				->select('productos.nombre', 'productos.id')
				->get();
			foreach ($productos as $key => $item) {
				$precios = DB::table('productos_precios')
					->select('productos_precios.valor')
					->selectRaw("(SELECT CONCAT('https://prueba-coex.s3.amazonaws.com/',imagen) FROM productos INNER JOIN productos_atributos_imagenes ON productos_atributos_imagenes.id_producto = productos.id WHERE productos_atributos_imagenes.id_producto = $item->id LIMIT 1) AS imagen ")
					->where('productos_precios.id_producto', $item->id)
					->first();
				$item->precio = $precios;
			}
			foreach ($productos as $key => $item) {
				$productosIcon = DB::table('productos')
					->selectRaw("(SELECT CONCAT('https://prueba-coex.s3.amazonaws.com/',icono)) icono")
					->join('productos_tallas_aplicadas', 'productos_tallas_aplicadas.id_producto', '=', 'productos.id')
					->join('productos_tallas AS t1', 't1.id', '=', 'productos_tallas_aplicadas.id_talla')
					->join('productos_tallas AS t2', 't2.id', '=', 't1.id_padre')
					->join('productos_categorias', 't2.id', '=', 'productos_categorias.id_talla')
					->where('productos.id', $item->id)
					->first();
				$item->productosIcon = $productosIcon;
			}

			return [
				"productos" => $productos
			];
		} catch (\Throwable $th) {
			$this->capturar($th);
		}
	}
	function crearProductos(Request $request)
	{
		try {

			$validator = Validator::make($request->all(), [
				'nombre' => 'required',
				'descripcion' => 'required',
				'checked' => 'required|array|min:1',
				'checkedTallas' => 'required|array|min:1'
			]);
			if ($validator->fails()) {
				return response()->json($validator->errors()->toJson(), 400);
			}

			$producto = new Producto;
			$producto->nombre = $request->nombre;
			$producto->descripcion = $request->descripcion;
			$producto->created_by = auth()->id();
			$producto->save();

			return response()->json(array('success' => true, 'last_product_id' => $producto->id), 200);
		} catch (\Throwable $th) {
			return $this->capturar($th);
		}
	}
	function verProductos($id_producto)
	{
		try {
			$productodetail = DB::table('productos')
				->select('productos.id', 'productos.nombre', 'productos.descripcion')
				->selectRaw("(SELECT productos_categorias.nombre FROM productos_tallas_aplicadas INNER JOIN productos_tallas AS t1 ON t1.id = productos_tallas_aplicadas.id_talla INNER JOIN productos_tallas AS t2 ON t1.id_padre = t2.id INNER JOIN productos_categorias ON productos_categorias.id_talla=t2.id WHERE productos_tallas_aplicadas.id_producto = $id_producto LIMIT 1 ) AS tallaCategoria")
				->selectRaw("(SELECT t2.nombre FROM productos_tallas_aplicadas INNER JOIN productos_tallas AS t1 ON t1.id = productos_tallas_aplicadas.id_talla INNER JOIN productos_tallas AS t2 ON t1.id_padre = t2.id WHERE productos_tallas_aplicadas.id_producto = $id_producto LIMIT 1 ) AS tallaNombre")
				->where('productos.id', $id_producto)
				->first();

			$tipoAtributo = DB::table('productos_atributos_aplicados')
				->select('a2.nombre AS padre', 'a1.nombre AS hijo')
				->join('productos_atributos AS a1', 'a1.id', '=', 'productos_atributos_aplicados.id_atributo')
				->join('productos_atributos AS a2', 'a1.id_padre', '=', 'a2.id')
				->where('productos_atributos_aplicados.id_producto', $productodetail->id)
				->get();
			$productodetail->atributos = $tipoAtributo;

			$tipoTalla = DB::table('productos_tallas_aplicadas')
				->select('t2.nombre AS tallaPadre', 't1.nombre AS hijo')
				->join('productos_tallas AS t1', 't1.id', '=', 'productos_tallas_aplicadas.id_talla')
				->join('productos_tallas AS t2', 't1.id_padre', '=', 't2.id')
				->where('productos_tallas_aplicadas.id_producto', $productodetail->id)
				->get();
			$productodetail->tallas = $tipoTalla;

			$atributos = DB::table('productos')
				->select('a1.id AS id')
				->join('productos_atributos_aplicados', 'productos_atributos_aplicados.id_producto', '=', 'productos.id')
				->join('productos_atributos AS a1', 'productos_atributos_aplicados.id_atributo', '=', 'a1.id')
				->where('productos.id', $id_producto)
				->get();
			$atributosHijos = [];
			foreach ($atributos as $key => $item) {
				$id = $item->id;
				$atributosHijos[] = $id;
			}
			$tallas = DB::table('productos')
				->select('t1.id AS id')
				->join('productos_tallas_aplicadas', 'productos_tallas_aplicadas.id_producto', '=', 'productos.id')
				->join('productos_tallas AS t1', 'productos_tallas_aplicadas.id_talla', '=', 't1.id')
				->where('productos.id', $id_producto)
				->get();
			$tallasHijas = [];
			foreach ($tallas as $key => $item) {
				$id = $item->id;
				$tallasHijas[] = $id;
			}
			return ['productodetail' => $productodetail, 'atributosHijos' => $atributosHijos, 'tallasHijas' => $tallasHijas];
		} catch (\Throwable $th) {
			$this->capturar($th);
		};
	}
	function eliminarProducto($id)
	{
		try {
			$productDelete = Producto::find($id);
			$productDelete->delete();
		} catch (\Throwable $th) {
			$this->capturar($th);
		}
	}
	function editarProducto(Request $request, $id)
	{
		try {

			$validator = Validator::make($request->all(), [
				'nombre' => 'required',
				'descripcion' => 'required',

			]);
			if ($validator->fails()) {
				return response()->json($validator->errors()->toJson(), 400);
			}

			$producto = Producto::FindOrFail($id);
			$producto->nombre = $request->nombre;
			$producto->descripcion = $request->descripcion;
			$producto->updated_by = auth()->id();
			$producto->save();
		} catch (\Throwable $th) {
			$this->capturar($th);
		}
	}

	//busca producto por nombre
	function busquedaProducto($buscar)
	{
		try {
			$producto_busqueda = DB::table('productos')
				->select('*')
				->where('productos.nombre', 'like', '%' . $buscar . '%')
				->get();
			foreach ($producto_busqueda as $key => $item) {
				$precios = DB::table('productos_precios')
					->select('productos_precios.valor')
					->selectRaw("(SELECT CONCAT('https://prueba-coex.s3.amazonaws.com/',imagen) FROM productos INNER JOIN productos_atributos_imagenes ON productos_atributos_imagenes.id_producto = productos.id WHERE productos_atributos_imagenes.id_producto = $item->id LIMIT 1) AS imagen ")
					->where('productos_precios.id_producto', $item->id)
					->first();
				$item->precio = $precios;
			}
			foreach ($producto_busqueda as $key => $item) {
				$productosIcon = DB::table('productos')
					->selectRaw("(SELECT CONCAT('https://prueba-coex.s3.amazonaws.com/',icono)) icono")
					->join('productos_tallas_aplicadas', 'productos_tallas_aplicadas.id_producto', '=', 'productos.id')
					->join('productos_tallas AS t1', 't1.id', '=', 'productos_tallas_aplicadas.id_talla')
					->join('productos_tallas AS t2', 't2.id', '=', 't1.id_padre')
					->join('productos_categorias', 't2.id', '=', 'productos_categorias.id_talla')
					->where('productos.id', $item->id)
					->first();
				$item->productosIcon = $productosIcon;
			}
			return ['productos' => $producto_busqueda];
		} catch (\Throwable $th) {
			$this->capturar($th);
		}
	}
	//lista las tallas al crear un producto
	public function getTallas($id)
	{
		try {
			$tallaPadre = DB::table('productos_categorias')
				->select('productos_tallas.id', 'productos_tallas.nombre', 'productos_categorias.nombre')
				->join('productos_tallas', 'productos_categorias.id_talla', '=', 'productos_tallas.id')
				->where('productos_categorias.id', $id)
				->get();
			foreach ($tallaPadre as $key => $item) {
				$tallaHijas = DB::table('productos_tallas')
					->select('productos_tallas.id', 'productos_tallas.id_padre', 'productos_tallas.nombre')
					->where('productos_tallas.id_padre', $item->id)
					->get();
				$item->subTallas = $tallaHijas;
			}
			return ['tallasHijas' => $tallaHijas];
		} catch (\Exception $e) {
			return $this->capturar($e, 'Error');
		}
	}

	//lista los atributos al crear un producto
	public function getAtributos($id)
	{
		try {
			$subAtributos = DB::table('productos_atributos')
				->select('productos_atributos.id', 'productos_atributos.nombre')
				->where('productos_atributos.id_padre', $id)
				->get();
			return ['subAtributos' => $subAtributos];
		} catch (\Exception $e) {
			return $this->capturar($e, 'Error');
		}
	}

	//ordena index productos por nombre
	public function orderByName()
	{
		try {
			$productosOrder = DB::table('productos')
				->select('productos.nombre', 'productos.id')
				->orderBy('nombre', 'asc')
				->get();
			foreach ($productosOrder as $key => $item) {
				$precios = DB::table('productos_precios')
					->select('productos_precios.valor')
					->selectRaw("(SELECT CONCAT('https://prueba-coex.s3.amazonaws.com/',imagen) FROM productos INNER JOIN productos_atributos_imagenes ON productos_atributos_imagenes.id_producto = productos.id WHERE productos_atributos_imagenes.id_producto = $item->id LIMIT 1) AS imagen ")
					->where('productos_precios.id_producto', $item->id)
					->first();
				$item->precio = $precios;
			}
			foreach ($productosOrder as $key => $item) {
				$productosIcon = DB::table('productos')
					->selectRaw("(SELECT CONCAT('https://prueba-coex.s3.amazonaws.com/',icono)) icono")
					->join('productos_tallas_aplicadas', 'productos_tallas_aplicadas.id_producto', '=', 'productos.id')
					->join('productos_tallas AS t1', 't1.id', '=', 'productos_tallas_aplicadas.id_talla')
					->join('productos_tallas AS t2', 't2.id', '=', 't1.id_padre')
					->join('productos_categorias', 't2.id', '=', 'productos_categorias.id_talla')
					->where('productos.id', $item->id)
					->first();
				$item->productosIcon = $productosIcon;
			}
			return ['productos' => $productosOrder];
		} catch (\Exception $e) {
			return $this->capturar($e, 'Error');
		}
	}


	//ordena index productos por actualizacion
	public function orderByUpdate()
	{
		try {
			$productosOrder = DB::table('productos')
				->select('productos.nombre', 'productos.id')
				->orderBy('updated_at', 'desc')
				->get();
			foreach ($productosOrder as $key => $item) {
				$precios = DB::table('productos_precios')
					->select('productos_precios.valor')
					->selectRaw("(SELECT CONCAT('https://prueba-coex.s3.amazonaws.com/',imagen) FROM productos INNER JOIN productos_atributos_imagenes ON productos_atributos_imagenes.id_producto = productos.id WHERE productos_atributos_imagenes.id_producto = $item->id LIMIT 1) AS imagen ")
					->where('productos_precios.id_producto', $item->id)
					->first();
				$item->precio = $precios;
			}
			foreach ($productosOrder as $key => $item) {
				$productosIcon = DB::table('productos')
					->selectRaw("(SELECT CONCAT('https://prueba-coex.s3.amazonaws.com/',icono)) icono")
					->join('productos_tallas_aplicadas', 'productos_tallas_aplicadas.id_producto', '=', 'productos.id')
					->join('productos_tallas AS t1', 't1.id', '=', 'productos_tallas_aplicadas.id_talla')
					->join('productos_tallas AS t2', 't2.id', '=', 't1.id_padre')
					->join('productos_categorias', 't2.id', '=', 'productos_categorias.id_talla')
					->where('productos.id', $item->id)
					->first();
				$item->productosIcon = $productosIcon;
			}
			return ['productos' => $productosOrder];
		} catch (\Exception $e) {
			return $this->capturar($e, 'Error');
		}
	}
	//muestra los productos de la categoria seleccionada
	public function orderByCAtegory($id)
	{
		try {
			$productosOrder = DB::table('productos_categorias')
				->select('productos.nombre', 'productos.id')
				->join('productos_tallas AS t1', 'productos_categorias.id_talla', '=', 't1.id')
				->join('productos_tallas AS t2', 't1.id', '=', 't2.id_padre')
				->join('productos_tallas_aplicadas', 't2.id', '=', 'productos_tallas_aplicadas.id_talla')
				->join('productos', 'productos.id', '=', 'productos_tallas_aplicadas.id_producto')
				->where('productos_categorias.id', $id)
				->groupBy('productos.id')
				->get();
			foreach ($productosOrder as $key => $item) {
				$precios = DB::table('productos_precios')
					->select('productos_precios.valor')
					->selectRaw("(SELECT CONCAT('https://prueba-coex.s3.amazonaws.com/',imagen) FROM productos INNER JOIN productos_atributos_imagenes ON productos_atributos_imagenes.id_producto = productos.id WHERE productos_atributos_imagenes.id_producto = $item->id LIMIT 1) AS imagen ")
					->where('productos_precios.id_producto', $item->id)
					->first();
				$item->precio = $precios;
			}
			foreach ($productosOrder as $key => $item) {
				$productosIcon = DB::table('productos')
					->selectRaw("(SELECT CONCAT('https://prueba-coex.s3.amazonaws.com/',icono)) icono")
					->join('productos_tallas_aplicadas', 'productos_tallas_aplicadas.id_producto', '=', 'productos.id')
					->join('productos_tallas AS t1', 't1.id', '=', 'productos_tallas_aplicadas.id_talla')
					->join('productos_tallas AS t2', 't2.id', '=', 't1.id_padre')
					->join('productos_categorias', 't2.id', '=', 'productos_categorias.id_talla')
					->where('productos.id', $item->id)
					->first();
				$item->productosIcon = $productosIcon;
			}
			return ['productos' => $productosOrder];
		} catch (\Exception $e) {
			return $this->capturar($e, 'Error');
		}
	}
}
