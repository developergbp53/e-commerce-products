<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ProductosPrecio;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class productosPreciosController extends Controller
{
	function crearProductos_precio(Request $request)
	{
		try {

			$validator = Validator::make($request->all(), [
				'desde' => 'required',
				'valor' => 'required'
			]);
			if ($validator->fails()) {
				return response()->json($validator->errors()->toJson(), 400);
			}

			$producto_precio = new ProductosPrecio;
			$producto_precio->desde = $request->desde;
			$producto_precio->valor = $request->valor;
			$producto_precio->id_producto = $request->id_producto;
			$producto_precio->created_by = auth()->id();
			$producto_precio->save();
		} catch (\Exception $e) {
			return $this->capturar($e, 'Error');
		}
	}
	public function getPrecioPorId($id)
	{
		try {
			$producto_precio_id = DB::table('productos_precios')
				->select('productos_precios.id', 'productos_precios.desde', 'productos_precios.valor')
				->join('productos', 'productos.id', '=', 'productos_precios.id_producto')
				->where('productos_precios.id_producto', $id)
				->get();
			return ['producto_precio_id' => $producto_precio_id];
		} catch (\Exception $e) {
			return $this->capturar($e, 'Error');
		}
	}
	function eliminarPrecio($id)
	{
		try {
			$precioDelete = ProductosPrecio::find($id);
			$precioDelete->delete();
		} catch (\Exception $e) {
			return $this->capturar($e, 'Error');
		}
	}
	function editarPrecio(Request $request, $id)
	{
		try {

			$validator = Validator::make($request->all(), [
				'desde' => 'required',
				'valor' => 'required'
			]);
			if ($validator->fails()) {
				return response()->json($validator->errors()->toJson(), 400);
			}

			$precioEdit = ProductosPrecio::FindOrFail($id);
			$precioEdit->desde = $request->desde;
			$precioEdit->valor = $request->valor;
			$precioEdit->updated_by = auth()->id();
			$precioEdit->save();
		} catch (\Exception $e) {
			return $this->capturar($e, 'Error');
		}
	}
}
