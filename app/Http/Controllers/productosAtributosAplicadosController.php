<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ProductosAtributosAplicado;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class productosAtributosAplicadosController extends Controller
{
	function crearProductosAtributos(Request $request)
	{
		try {
			$atributos_productos = [];
			foreach ($request->id_atributos as $item) {
				$temp_data = [
					'id_atributo' => $item,
					'id_producto' => $request->idProducto,
					'created_by' => auth()->id()
				];
				$atributos_productos[] = $temp_data;
			}

			DB::table('productos_atributos_aplicados')->insert($atributos_productos);

			return response()->json(array('success' => true), 200);;
		} catch (\Throwable $th) {
			$this->capturar($th);
		}
	}
	function editCreate(Request $request)
	{

		try {
			$atributoProducto = new ProductosAtributosAplicado;
			$atributoProducto->id_producto = $request->idProducto;
			$atributoProducto->id_atributo = $request->atributo;
			$atributoProducto->created_by = auth()->id();
			$atributoProducto->save();
			return response()->json(array('success' => true), 200);
		} catch (\Throwable $th) {
			$this->capturar($th);
		}
	}
	function editDelete(Request $request)
	{

		try {

			$atributoDelete = DB::table('productos_atributos_aplicados')
				->select('id')
				->where('id_producto', $request->idProducto)
				->where('id_atributo', $request->atributo)
				->delete();

			return response()->json(array('success' => true), 200);
		} catch (\Throwable $th) {
			$this->capturar($th);
		}
	}
}
