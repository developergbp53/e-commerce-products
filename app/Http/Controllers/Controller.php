<?php

namespace App\Http\Controllers;

use App\Models\Errores;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Response;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $bucket;
    protected $bucketManager;

    public function __construct(){
        $this->bucket = "https://".config("filesystems.disks.s3.bucket").".s3.amazonaws.com/";
        $this->bucketManager = "https://".config("filesystems.disks.s3.bucket").".s3.amazonaws.com/";
    }
    //Funcion para paginar una colleccion o un array   By: Yesid ortiz
    public function paginate($items, $perPage = 10, $page = null, $options = []) {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        $data =  new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
        $set = $data->getCollection()->values();
        $data->setCollection(collect($set));
        return $data;
    }

    /**
     * Captura una excepción y genera un response a partir de ella
     *
     * @param  Exception $excepcion = Excepción capturada
     * @param  String $titulo (opcional) = Título del error desplegado para el cliente
     * @param  String $mensaje (opcional) = Mensaje del error desplegado para el cliente
     * @param  Int $http_status_code (opcional) = código de error HTTP, con el que responderá la petición (4xx)
     * @return Response
     */
    public function capturar($excepcion, String $titulo = null, String $mensaje = null, Int $http_status_code = 400) {

        /**
         * Para móvil, hay unas peticiones que el proyecto estan en el cuerpo de la petición
         * Esto genera que cuando es así da error porque intenta guardar en z_errores sin setear la db del proyecto
         *
         */

        $validationException = is_a($excepcion, 'Illuminate\Validation\ValidationException');
        $queryException = is_a($excepcion, 'Illuminate\Database\QueryException');

        if ($validationException) {
            $titulo = $titulo ?? 'Ha ocurrido un error al validar los datos';
            $mensaje = collect($excepcion->errors())->flatten()->join(' ');
        }

        if ($queryException) {
            $titulo = $titulo ?? 'Ha ocurrido un error al validar los datos';
            if ($excepcion->errorInfo && is_array($excepcion->errorInfo)) {
                switch ($excepcion->errorInfo[1]) {
                    case 1451:
                        $mensaje = 'El dato que deseas eliminar tiene registros asociados';
                        break;
                    case 1048:
                        $mensaje = 'Datos incompletos';
                        break;
                    default:
                        $mensaje = 'Hay un problema con los datos, inténtalo nuevamente';
                        break;
                }
            }
        }

        $http_status_code = $validationException || $http_status_code === 0 ? 422 : $http_status_code;
        $http_status_code = $queryException || $http_status_code === 0 ? 422 : $http_status_code;

        $tmp = [
            'url_origen' => url()->previous(),
            'url_destino' => url()->current(),
            'mensaje' => $excepcion->getMessage(),
            'error' => substr($excepcion, 0, 1999),
            'archivo' => $excepcion->getFile(),
            'linea' => $excepcion->getLine(),
            'proyecto' => 1,
            'parametros' => json_encode(request()->all()),
        ];
        if (auth()->check()) {
            $tmp['created_by'] = auth()->user()->id;
        }
        $errores = Errores::create($tmp);

        $response = [
            'timestamp'   => Carbon::now()->format('d/m/y h:i A'),
            'status'      => $http_status_code,
            'titulo'      => $titulo ?? 'Ha ocurrido un error al ejecutar la consulta',
            'mensaje'     => $mensaje ?? $excepcion->getMessage(),
            'error'       => "{$excepcion->getMessage()} - Archivo: {$excepcion->getFile()} - Línea: {$excepcion->getLine()}",
            'validaciones' => $validationException ? $excepcion->errors() : null,
            'reportado' => "Reportado en {$errores->id} tabla z_errores",
        ];

        return response($response, $http_status_code);
    }

    /**
     * susccesResponse
     *
     * @param  string $data
     * @param  int  $code
     * @return Illuminate\Http\JsonResponse
     */
    public function successResponse($data = [], $code = Response::HTTP_OK): Response {
        $exito = collect(['exito' => true]);
        $data = $exito->merge($data);
        return response($data, $code);
    }
    /**
     * errorResponse
     *
     * @param  string $message
     * @param  int $code
     * @return Illuminate\Http\JsonResponse
     */
    public function errorResponse($data = [], $code = 400): Response {
        $exito = collect(['exito' => false]);
        $data = $exito->merge($data);
        return response($data, $code);
    }

    public function guardarImagen($img, $dimension, $ruta, $mini = false, $dimension_mini = 100) {
        try {
            // return $img;
            $imagen_file = Image::make($img);
            $imagen_file_ancho = $imagen_file->width();
            $imagen_file_alto = $imagen_file->height();

            //se obtiene el lado maximo de la imagen
            if ($imagen_file_ancho > $imagen_file_alto) {
                $imagen_max = $imagen_file_ancho;
            } else {
                $imagen_max = $imagen_file_alto;
            }

            //Se obtiene el porcentaje para cambiar el tamaño de la imagen para que no se distorcione
            $imagen_porcentaje = ($dimension / $imagen_max);

            //Solo se cambia de tamaño cuando alguno de los lados pasa la dimension requerida
            if ($imagen_porcentaje < 1) {
                $imagen_alto = $imagen_file_alto * $imagen_porcentaje;
                $imagen_ancho = $imagen_file_ancho * $imagen_porcentaje;
                $imagen = $imagen_file->resize($imagen_ancho, $imagen_alto);
            } else {
                $imagen = Image::make($img);
            }

            $ex = explode('/', $imagen->mime);
            $ext = end($ex);
            $nombre_aleatorio = uniqid(rand(), true) . str_replace(" ", "", microtime()) . ".$ext";
            $subruta = ($ruta != '' ? "$ruta/" : '') . $nombre_aleatorio;
            $subruta_mini = '';
            Storage::put($subruta, (string)$imagen->encode($ext));
            if ($mini) {
                $imagen_mini = $this->formateaImagen($imagen, $dimension_mini);
                $nombre_aleatorio_mini = uniqid(rand(), true) . str_replace(" ", "", microtime()) . ".$ext";
                $subruta_mini = ($ruta != '' ? "$ruta/" : '') . $nombre_aleatorio_mini;
                Storage::put($subruta_mini, (string)$imagen_mini->encode($ext));
            }
            return [
                'estado' => true,
                'ruta' => $subruta,
                'ruta_mini' => $subruta_mini
            ];
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function getStoreFiles($file, $destinationPath = false) {
        try {
            if (isset($file)) {
                $response = [];
                $response['filename'] = $file->getClientOriginalName();
                $response['peso'] = $file->getSize();
                $response['ext'] = $file->getClientOriginalExtension();
                // $nombre_del_archivo = $response['filename'];
                $nombre_del_archivo = md5(uniqid('gbp-', true)) . "." . $response['filename'];

                return Storage::putFileAs($destinationPath, $file, $nombre_del_archivo);
            }
            return false;
        } catch (\Exception $e) {
            return false;
        }
    }

    public function formateaImagen($imagen, $porcentaje) {
        $imagen_file = Image::make($imagen);
        $imagen_file_ancho = $imagen_file->width();
        $imagen_file_alto = $imagen_file->height();

        if ($imagen_file_ancho > $imagen_file_alto) {
            $imagen_max = $imagen_file_ancho;
        } else {
            $imagen_max = $imagen_file_alto;
        }
        $imagen_porcentaje = ($porcentaje / $imagen_max);
        if ($imagen_porcentaje < 1) {
            $imagen_alto = $imagen_file_alto * $imagen_porcentaje;
            $imagen_ancho = $imagen_file_ancho * $imagen_porcentaje;
            $imagen = $imagen_file->resize($imagen_ancho, $imagen_alto);
        } else {
            $imagen = Image::make($imagen);
        }
        return $imagen;
    }

    public function deleteImagen($rutaImagen){
        try {
                Storage::delete($rutaImagen);
                return true;
        }catch (\Exception $th) {
            throw $th;
        }
    }
}
