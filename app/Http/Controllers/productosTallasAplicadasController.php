<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ProductosTallasAplicada;
use Illuminate\Support\Facades\DB;


class productosTallasAplicadasController extends Controller
{
	function crearProductosTallas(Request $request)
	{
		try {
			$tallas_productos = [];
			foreach ($request->id_tallas as $item) {


				$temp_data = [
					'id_talla' => $item,
					'id_producto' => $request->idProducto,
					'created_by' => auth()->id()
				];
				$tallas_productos[] = $temp_data;
			}

			DB::table('productos_tallas_aplicadas')->insert($tallas_productos);

			return response()->json(array('success' => true), 200);;
		} catch (\Throwable $th) {
			$this->capturar($th);
		}
	}
	function editCreate(Request $request)
	{

		try {
			$tallaProducto = new ProductosTallasAplicada;
			$tallaProducto->id_producto = $request->idProducto;
			$tallaProducto->id_talla = $request->talla;
			$tallaProducto->created_by = auth()->id();
			$tallaProducto->save();
			return response()->json(array('success' => true), 200);
		} catch (\Throwable $th) {
			$this->capturar($th);
		}
	}
	function editDelete(Request $request)
	{

		try {

			$tallaDelete = DB::table('productos_tallas_aplicadas')
				->select('id')
				->where('id_producto', $request->idProducto)
				->where('id_talla', $request->talla)
				->delete();

			return response()->json(array('success' => true), 200);
		} catch (\Throwable $th) {
			$this->capturar($th);
		}
	}
}
