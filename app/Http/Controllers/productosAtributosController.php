<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ProductosAtributo;
use Illuminate\Support\Facades\DB;


class productosAtributosController extends Controller
{
	function crearAtributo(Request $request)
	{
		try {
			$producto_talla = new ProductosAtributo;
			$producto_talla->nombre = $request->atributoNombre;
			$producto_talla->posicion = $request->posicion;
			$producto_talla->created_by = auth()->id();
			$producto_talla->save();
			return response()->json(array('success' => true), 200);
		} catch (\Exception $e) {
			return $this->capturar($e, 'Error');
		}
	}
	function crearSubAtributo(Request $request)
	{
		try {
			$producto_atributo = new ProductosAtributo;
			$producto_atributo->nombre = $request->subAtributoNombre;
			$producto_atributo->posicion = $request->posicion;
			$producto_atributo->id_padre = $request->id_padre;
			$producto_atributo->created_by = auth()->id();
			$producto_atributo->save();
			return response()->json(array('success' => true), 200);
		} catch (\Exception $e) {
			return $this->capturar($e, 'Error');
		}
	}
	public function getAtributosPadre()
	{
		try {
			$atributoPadre = DB::table('productos_atributos')
				->select('productos_atributos.id', 'productos_atributos.nombre', 'productos_atributos.posicion',)
				->where('productos_atributos.id_padre', null)
				->get();
			foreach ($atributoPadre as $key => $item) {
				$atributoHijos = DB::table('productos_atributos')
					->select('productos_atributos.id', 'productos_atributos.id_padre', 'productos_atributos.nombre', 'productos_atributos.posicion',)
					->selectRaw("(SELECT COUNT(id_producto) FROM productos_atributos_aplicados WHERE productos_atributos.id = productos_atributos_aplicados.id_atributo  GROUP BY id_atributo) AS ProductosCount")
					->where('productos_atributos.id_padre', $item->id)
					->get();
				$item->subAtributos = $atributoHijos;
			}
			return ['atributos' => $atributoPadre];
		} catch (\Exception $e) {
			return $this->capturar($e, 'Error');
		}
	}
	public function eliminarAtributo($id)
	{
		try {
			$atributoDelete = ProductosAtributo::find($id);
			$atributoDelete->delete();
			return response()->json(array('success' => true), 200);
		} catch (\Exception $e) {
			return $this->capturar($e, 'Error');
		}
	}
	public function editarAtributo(Request $request, $id)
	{
		try {
			$atributoEdit = ProductosAtributo::FindOrFail($id);
			$atributoEdit->nombre = $request->nombre;
			$atributoEdit->posicion = $request->posicion;
			$atributoEdit->updated_by = auth()->id();
			$atributoEdit->save();
			return response()->json(array('success' => true), 200);
		} catch (\Exception $e) {
			return $this->capturar($e, 'Error');
		}
	}
}
